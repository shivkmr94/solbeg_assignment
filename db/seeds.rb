# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


admin_user = User.create!(name: 'Admin',email: 'admin@solbegindia.com', password: 'admin@solbeg', password_confirmation: 'admin@solbeg', role: 'admin' )
admin_user.create_address({line_1: 'address line1 1', line_2: 'address line1 2', city: 'address city 1', state: 'address city 1', pincode: '123456', landmark: 'address landmark 1', country: 'address country 1', mobile_number: '8454997011'})


[{name: 'Shiv', email: 'shivkmr94112@gmail.com', address_attributes: {line_1: 'address line1 1', line_2: 'address line1 2', city: 'address city 1', state: 'address city 1', pincode: '123456', landmark: 'address landmark 1', country: 'address country 1', mobile_number: '8454997011'}}, {name: 'Shiv1', email: 'shivkmr94212@gmail.com', address_attributes: {line_1: 'address line1 1', line_2: 'address line1 2', city: 'address city 1', state: 'address city 1', pincode: '123456', landmark: 'address landmark 1', country: 'address country 1', mobile_number: '8454997011'}}, {name:'Shiv2', email:'shivkmr9422@gmail.com', address_attributes: {line_1: 'address line1 1', line_2: 'address line1 2', city: 'address city 1', state: 'address city 1', pincode: '123456', landmark: 'address landmark 1', country: 'address country 1', mobile_number: '8454997011'}}, {name:'Shiv123', email:'shiv123@gmail.com'}, {name:'shiv12',  email:'shiv12@gmail.com', address_attributes: {line_1: 'address line1 1', line_2: 'address line1 2', city: 'address city 1', state: 'address city 1', pincode: '123456', landmark: 'address landmark 1', country: 'address country 1', mobile_number: '8454997011'}}].each do |user_hsh|
  user = User.find_or_initialize_by(email: user_hsh[:email])
  user.name = user_hsh[:name]
  user.password = 'password'
  user.password_confirmation = 'password'
  user.build_address(user_hsh[:address_attributes])
  user.save!
end