module ApplicationHelper

  def date_formatter(date)
    date.to_date.strftime('%b %d %Y') rescue ''
  end
end
