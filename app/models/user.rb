class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  enum role: [:user, :admin]
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_one :address, dependent: :destroy, autosave: true

  validates :name, :presence => true
  validates :email,
            :uniqueness => {:message => "Another account is already using this email address"},
            :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i,
            :message=> "Invalid Email Id" },
            :presence =>true

  has_one_attached :profile_picture
  accepts_nested_attributes_for :address

  after_initialize :set_default_role, :if => :new_record?

  def set_default_role
    self.role ||= :user
  end

  def full_address
    self.address.try(:full_address)
  end
end
