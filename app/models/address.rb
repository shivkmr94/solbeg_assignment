class Address < ApplicationRecord
  belongs_to :user

  def full_address
    str = self.line_1
    ['line_2', 'landmark', 'pincode', 'city', 'state', 'country'].each do |attrr|
      str += ", #{self.try(attrr)}" if self.try(attrr).present?
    end
    str
  end
end