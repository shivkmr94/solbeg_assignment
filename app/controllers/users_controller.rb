class UsersController < ApplicationController
  before_action :verify_admin
  before_action :set_user, only: [:edit, :update]
  after_action :verify_authorized

  def index
    authorize User
    @users = User.user.order('id desc').with_attached_profile_picture
    @users = @users.paginate(:page => params[:page], :per_page => 10)
  end

  def edit
    authorize @user
  end

  def update
    authorize @user
    if @user.update(user_update_params)
      redirect_to users_path, notice: 'User was successfully updated.'
    else
      render :edit
    end
  end

  private
    def user_update_params
      params.require(:user).permit(policy(User).permitted_attributes)
    end

    def set_user
      @user = User.where(id: params[:id]).first
    end

    def verify_admin
      redirect_to '/' unless current_user.admin?
    end
end
