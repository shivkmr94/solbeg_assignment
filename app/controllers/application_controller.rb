class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery
  # before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized


  protected

  # def configure_permitted_parameters
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :email, :password, :profile_picture, :address_attributes=> [:line_1, :line_2, :landmark, :pincode, :city, :state]] )
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:name, :profile_picture, :address_attributes=> [:id, :line_1, :line_2, :city, :state, :pincode, :landmark]])
  # end

  def after_sign_in_path_for(resource)
    if resource.present? && resource.admin?
      users_path
    else
      root_path
    end
  end

  private

  def user_not_authorized
    flash[:alert] = "You are not authorized to perform this action."
    redirect_to(request.referrer || root_path)
  end
end
