class UserPolicy < ApplicationPolicy

  def index?
    user.admin?
  end

  def edit?
    # user = User.last(2).first
    user.admin? || user == record
  end

  def update?
    user.admin? || user == record
  end

  # def permitted_update_attributes
  #   [:address_attributes=> [:id, :line_1, :line_2, :city, :state, :pincode, :landmark, :country, :mobile_number]]
  # end

  # def permitted_profile_update_attributes

  # end

  # def permitted_attributes_for_edit
  #   []
  # end

  # def permitted_attributes_for_update
  #   []
  # end

  def permitted_attributes
    if user.admin?
      [:profile_picture, :address_attributes=> [:id, :line_1, :line_2, :city, :state, :pincode, :landmark, :country, :mobile_number]]
    else
      [:profile_picture, :address_attributes=> [:id, :line_1, :line_2, :city, :state, :pincode, :landmark, :country, :mobile_number]]
    end
  end
end