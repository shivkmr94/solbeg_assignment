Rails.application.routes.draw do
  # devise_for :users, :path => 'auth', controllers: {
  #   registrations: 'devise/registrations'
  #   }
  devise_for :users, :path => 'auth', controllers: { registrations: 'users/registrations' }
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :users#, only: [:index]

  devise_scope :user do
    root to: "users/registrations#edit"
  end
end
